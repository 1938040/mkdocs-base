"use strict"

document.addEventListener("DOMContentLoaded", setup);

let p=document.querySelector('#quote');
//adds event lister to getquote button
function setup(){
    let btn=document.querySelector('#btn');

    btn.addEventListener('click',getquote);
}
//when getquote button is clicked fetch api
function getquote(){
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
        .then(check)
        .then(display)
        .catch(Derror);
}

//check if response is good
function check(response){
    if(response.ok){
        return response.json();
    }
    throw new Error('Status code: '+response.statusCode);
}
//display the quote if response is good
function display(json){
    let quote=json[0]
    quote="Quote: "+JSON.stringify(quote);
    p.textContent=quote;
    p.classList.remove("error");
    p.classList.add("good");
}
//adds error text
function Derror(error){
    error='Please try again, Error: '+error;
    p.textContent=error;
    p.classList.remove("good");
    p.classList.add("error");
}